<?php

namespace App;

use App\Enum\ProgressionType;
use App\Helper\Progression;
use Exception;

class ProgressionApp {
    /** @var Progression */
    private $progressionHelper;

    public function __construct() {
        $this->progressionHelper = new Progression();
    }

    public function start() {
        while (true) {
            echo "Enter a progression string:" . PHP_EOL;
            $inputString = readline();
            readline_add_history($inputString);

            try {
                $progression_type = $this->progressionHelper->checkProgressionString($inputString);
                if (ProgressionType::ARITHMETIC_PROGRESSION === $progression_type) {
                    echo "It is an arithmetic progression" . PHP_EOL;
                } elseif (ProgressionType::GEOMETRIC_PROGRESSION === $progression_type) {
                    echo "It is a geometric progression" . PHP_EOL;
                }
            } catch (Exception $exception) {
                echo "It isn't a progression" . PHP_EOL;
            }
            echo "*********************************" . PHP_EOL;
        }
    }
}