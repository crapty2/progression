<?php

namespace App\Enum;

class ProgressionType {
    const NOT_PROGRESSION        = 0;
    const ARITHMETIC_PROGRESSION = 1;
    const GEOMETRIC_PROGRESSION  = 2;
}