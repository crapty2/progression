<?php

namespace App\Helper;

use App\Enum\ProgressionType;
use Exception;

class Progression {
    private $precision = 0.0001;

    /**
     * @param float[] $items
     *
     * @return int
     * @throws Exception
     */
    public function checkProgression($items) {
        if (count($items) >= 2) {
            if ($this->isArithmeticProgression($items)) {
                return ProgressionType::ARITHMETIC_PROGRESSION;
            }

            if ($this->isGeometricProgression($items)) {
                return ProgressionType::GEOMETRIC_PROGRESSION;
            }
        }

        throw new Exception("isn't a progression");
    }

    /**
     * @param string $progressionString
     * @param string $delimiter
     *
     * @return int
     */
    public function checkProgressionString($progressionString, $delimiter = ',') {
        $items = $this->parseProgression($progressionString, $delimiter);

        return $this->checkProgression($items);
    }

    /**
     * @param float[] $items
     *
     * @return bool
     */
    public function isArithmeticProgression($items) {
        $diff = $items[1] - $items[0];

        $stepCount = count($items) - 1;
        for ($i = 1; $i < $stepCount; $i++) {
            $current = $items[$i];
            $next = $items[$i + 1];
            if (abs($diff - ($next - $current)) >= $this->precision) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param float[] $items
     *
     * @return bool
     */
    public function isGeometricProgression($items) {
        if ($items[0] === 0.0 || $items[1] === 0.0) {
            return false;
        }

        $diff = $items[1] / $items[0];

        $stepCount = count($items) - 1;
        for ($i = 1; $i < $stepCount; $i++) {
            $current = $items[$i];
            $next = $items[$i + 1];
            if (abs($diff - ($next / $current)) >= $this->precision) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $progressionString
     * @param string $delimiter
     *
     * @return float[]
     */
    private function parseProgression($progressionString, $delimiter) {
        $items = explode($delimiter, $progressionString);
        $items = array_map(function ($item) {
            return (float)$item;
        }, $items);

        return $items;
    }
}