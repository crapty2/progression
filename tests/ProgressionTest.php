<?php

use App\Helper\Progression;
use App\Enum\ProgressionType as PgType;
use PHPUnit\Framework\TestCase;

class ProgressionTest extends TestCase {
    /**
     * @var Progression
     */
    protected static $pgH;

    public static function setUpBeforeClass() {
        self::$pgH = new Progression();
    }

    public function testArithmeticProgression() {
        $this->assertEquals(
            self::$pgH->isArithmeticProgression([1, 3, 5.0, 7]),
            true
        );

        $this->assertEquals(
            self::$pgH->isArithmeticProgression([9, 4, -1.0]),
            true
        );
    }

    public function testInvalidArithmeticProgression() {
        $this->assertEquals(
            self::$pgH->isArithmeticProgression([9, 4, 0]),
            false
        );
    }

    public function testGeometricProgression() {
        $this->assertEquals(
            self::$pgH->isGeometricProgression([1, 3, 9.0, 27]),
            true
        );

        $this->assertEquals(
            self::$pgH->isGeometricProgression([27, 9, 3.0, 1]),
            true
        );
    }

    public function testInvalidGeometricProgression() {
        $this->assertEquals(
            self::$pgH->isGeometricProgression([9, 4, 0]),
            false
        );
    }

    /**
     * @dataProvider invalidProgressionProvider
     * @expectedException Exception
     */
    public function testInvalidProgression($items) {
        self::$pgH->checkProgression($items);
    }

    public function invalidProgressionProvider() {
        return [
            [[]],
            [[1]],
            [[1, 3, 4, 6, 7]],
            [[1, 3, 0, 4, 6, 7]]
        ];
    }

    public function testProgressionString() {
        $this->assertEquals(
            self::$pgH->checkProgressionString('0.11,1.11,2.11,3.11,4.11'),
            PgType::ARITHMETIC_PROGRESSION
        );

        $this->assertEquals(
            self::$pgH->checkProgressionString('0.11, 0.011, 0.0011, 0.00011, 0.000011'),
            PgType::GEOMETRIC_PROGRESSION
        );
    }

    /**
     * @expectedException Exception
     */
    public function testInvalidProgressionString() {
        self::$pgH->checkProgressionString("1,2,3,error");
    }

    public static function tearDownAfterClass() {
        self::$pgH = null;
    }
}
